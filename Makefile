# Default Makefile for a Golang project

.PHONY: version verify test vet lint deps build docker push-docker

# Info
NAME=sandbox
VERSION := $(shell git describe --abbrev=0 --tags || echo unknown)
REVISION := $(shell git rev-parse --short HEAD || echo unknown)
#BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

# Docker
DOCKER_HOST=172.17.0.1:4243
DOCKER_VERSION=1.10.3

# GO
GO_LDFLAGS ?= "-X main.Name $(NAME) -X main.Version $(VERSION)"
GO_LDFLAGS_DEV ?= "-X main.Name $(NAME) -X main.Version $(VERSION) -X main.Revision $(REVISION)"
BUILD_PLATFORMS ?= "linux/amd64"
export GO15VENDOREXPERIMENT := 1

help:
	# make version - show information about current version
	# make verify - run vet, lint and test
	# make deps - install dependencies
	# make build - build artifacts
	# make docker - build docker container
	# make build-develop - build artifacts (staging)
	# make docker-develop - build docker container (staging)
version:
	@echo Name: $(NAME)
	@echo Version: $(VERSION)
	@echo Revision: $(REVISION)

verify: vet lint test

test:
	@go test $(glide novendor)

vet:
	@go vet *.go

lint:
	@golint *.go

deps:
	@go get -u github.com/golang/lint/golint
	@go get github.com/mitchellh/gox
	@go get github.com/Masterminds/glide
	@glide install
	@curl -fsSL -o /usr/local/bin/docker https://get.docker.com/builds/Linux/x86_64/docker-$(DOCKER_VERSION)
	@chmod +x /usr/local/bin/docker

build:
	gox -osarch=$(BUILD_PLATFORMS) \
    	-ldflags=$(GO_LDFLAGS) \
		-output="builds/$(NAME)-{{.OS}}-{{.Arch}}"

docker:
	@DOCKER_HOST=$(DOCKER_HOST) docker build -t $(NAME):$(VERSION) .

push-docker:

build-develop:
	gox -osarch=$(BUILD_PLATFORMS) \
    	-ldflags=$(GO_LDFLAGS_DEV) \
		-output="builds/$(NAME)-{{.OS}}-{{.Arch}}"

docker-develop:
	@DOCKER_HOST=$(DOCKER_HOST) docker build -t $(NAME):$(REVISION) .

push-docker-develop:

#proto:
#	protoc --go_out=. sandbox.proto
#
#clean:
#
#doc:
#	godoc -http=:8080 -index
#
#MAINTAINER=davy.melina@gmail.com
#VENDOR=DM
#LICENSE="BSD"
#URL="https://gitlab.com/dmelina/sandbox"
#DESCRIPTION="Sandbox for testing CI"
#
#deb: 
#	fpm --verbose -f -s dir -t deb \
#	-n ${NAME} \
#	-p pkg/ \
#	--maintainer ${MAINTAINER} \
#	--vendor ${VENDOR} \
#	--version ${VERSION} \
#	--url ${URL} \
#	--license ${LICENSE} \
#	--description ${DESCRIPTION} \
#	--category misc \
#	-a amd64 \
#	--after-install deb/postinst \
#	deb/init=/etc/init.d/${NAME} \
#	bin/${NAME}=/usr/local/sbin/${NAME} \
#	bin/${NAME}-cli=/usr/local/bin/${NAME}-cli \
#	bin/${NAME}-bench=/usr/local/bin/${NAME}-bench

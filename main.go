package main

import (
	"fmt"
	"os"

	"github.com/codegangsta/cli"
)

var (
	// Name represents the package name
	Name = "sandbox"
	// Version represents the package version
	Version = "dev"
	// Revision represents the last git commit hash
	Revision = ""
)

// FullVersion returns the full package version
func FullVersion() string {

	if Revision != "" {
		Revision = fmt.Sprintf(" (commit: %s)", Revision)
	}
	return fmt.Sprintf("%s%s", Version, Revision)
}

func main() {
	app := cli.NewApp()
	app.Name = Name
	app.Version = FullVersion()
	app.Usage = "Sandbox CI test"
	app.Run(os.Args)
}
